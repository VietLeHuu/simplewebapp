package webapp.utils;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import webapp.beans.UserAccount;

public class MyUtils {

	public static final String ATT_NAME_CONNECTION = "ATTRIBUTE_FOR_CONNECTION";

	private static final String ATT_NAME_USER_NAME = "ATTRIBUTE_FOR_STORE_USER_NAME_IN_COOKIE";

	//Store Connection into attribute of request
	public static void storeConnection(ServletRequest request, Connection conn){
		request.setAttribute(ATT_NAME_CONNECTION, conn);
	}

	//Get Connection stored in attribute of reuqest
	public static Connection getStoredConnection(ServletRequest request){
		Connection conn = (Connection) request.getAttribute(ATT_NAME_CONNECTION);
		return conn;
	}

	//Store logged in User info into Session
	public static void storeLoggedInUser(HttpSession session, UserAccount loggedInUser){
		session.setAttribute("loggedInUser", loggedInUser);
	}

	//Get UserInfo stored in Session
	public static UserAccount getLoggedInUser(HttpSession session){
		UserAccount loggedInUser = (UserAccount) session.getAttribute("loggedInUser");
		return loggedInUser;
	}

	//Store User Info into Cookie
	public static void storeUserCookie(HttpServletResponse response, UserAccount user){
		System.out.println("Store User Info Cookie:");

		Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, user.getUserName());

		cookieUserName.setMaxAge(24*60*60);

		response.addCookie(cookieUserName);
	}

	//Get user cookie
	public static String getUserNameFromCookie(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(Cookie cookie: cookies){
				if(ATT_NAME_USER_NAME.equals(cookie.getName())){
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	//delete user cookie
	public static void deleteUserCookie(HttpServletResponse response){
		Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, null);

		cookieUserName.setMaxAge(0);

		response.addCookie(cookieUserName);
	}
}
