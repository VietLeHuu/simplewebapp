package webapp.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import webapp.beans.Product;
import webapp.beans.UserAccount;

public class DBUtils {

	//find user by username and password
	public static UserAccount findUser(Connection conn, String userName, String password)throws SQLException{
		String sql = "Select a.User_Name, a.Password, a.Gender from User_Account a"
				+ "where a.User_Name = ? and a.Password = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		pstm.setString(2, password);
		ResultSet rs = pstm.executeQuery();

		if(rs.next()){
			String gender = rs.getString("Gender");
			UserAccount user = new UserAccount();
			user.setUserName(userName);
			user.setPassword(password);
			user.setGender(gender);
			return user;
		}
		return null;
	}

	//find user by username
	public static UserAccount findUser(Connection conn, String userName)throws SQLException{
		String sql = "Select a.User_Name, a.Password, a.Gender from User_Account a"
				+ "where a.User_Name = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, userName);
		ResultSet rs = pstm.executeQuery();

		if(rs.next()){
			String gender = rs.getString("Gender");
			String password = rs.getString("Password");
			UserAccount user = new UserAccount();
			user.setUserName(userName);
			user.setPassword(password);
			user.setGender(gender);
			return user;
		}
		return null;
	}

	//query all prodcut from db
	public static List<Product> queryProduct(Connection conn)throws SQLException{
		String sql ="Select p.Code, p.Name, p.Price from Product p";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<Product> list = new ArrayList<Product>();

		while(rs.next()){
			String code = rs.getString("Code");
			String name = rs.getString("Name");
			float price = rs.getFloat("Price");
			Product product = new Product(code, name,price);
			list.add(product);
		}
		return list;
	}

	//find a product by code
	public static Product findProduct(Connection conn, String code)throws SQLException{
		String sql = "Select * from Product p where p.Code=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1,code);

		ResultSet rs = pstm.executeQuery();

		while(rs.next()){
			String name = rs.getString("Name");
			float price = rs.getFloat("Price");
			Product product = new Product(code, name, price);
			return product;
		}
		return null;
	}


	public static boolean updateProduct(Connection conn, Product product)throws SQLException{
		String sql = "update Product set Name = ?, Price = ? where Code = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1, product.getName());
		pstm.setFloat(2, product.getPrice());
		pstm.setString(3, product.getCode());

		try{
			pstm.executeQuery();
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static boolean deleteProduct(Connection conn, String code) throws SQLException{
		String sql="delete from Product where Code = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1,code);

		try{
			pstm.executeQuery();
			return true;
		}catch(Exception e){
			return false;
		}

	}

}
