package webapp.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnUtils {

	public static Connection getMySQLConnection()throws SQLException, ClassNotFoundException{
		String hostName = "localhost";
		String dbName = "java_learning";
		String userName = "root";
		String password = "root";
		return getMySQLConnection(hostName, dbName, userName, password);
	}

	public static Connection getMySQLConnection(String hostName, String dbName, String userName, String password)throws SQLException, ClassNotFoundException{
		Class.forName("com.mysql.jdbc.Driver");

		//String connURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?verifyServerCertificate=false&useSSL=true";
		String connURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?verifyServerCertificate=false&useSSL=true";

		Connection conn = DriverManager.getConnection(connURL, userName, password);
		return conn;

	}
}
