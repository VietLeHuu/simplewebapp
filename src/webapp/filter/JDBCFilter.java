package webapp.filter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import webapp.conn.ConnectionUtils;
import webapp.utils.MyUtils;

import com.mysql.jdbc.Connection;

@WebFilter(filterName ="jdbcFilter", urlPatterns ={"/*"})

public class JDBCFilter implements Filter{

	public JDBCFilter(){

	}

	@Override
	public void init(FilterConfig fConfig)throws ServletException{

	}

	@Override
	public void destroy(){

	}

	//check if current request is a servlet?
	private boolean needJDBC(HttpServletRequest request){
		System.out.println("JDBC Filter");

		String servletPath = request.getServletPath();

		String pathInfo = request.getPathInfo();

		String urlPattern = servletPath;

		if(pathInfo != null){
			// -> /spath/*
			urlPattern = servletPath + "/*";
		}

		//Key: servletName
		//Value: ServletRegistration
		Map<String,? extends ServletRegistration> servletRegistrations = request.getServletContext().getServletRegistrations();

		// Collection of all servlet in your WebApp
		Collection<? extends ServletRegistration> values = servletRegistrations.values();
		for(ServletRegistration sr: values){
			Collection<String> mappings = sr.getMappings();
			if(mappings.contains(urlPattern)){
				return true;
			}
		}
		return false;
	}


	//filter request from user
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException{
		//ep kieu thanh HttpServletRequest
		HttpServletRequest req = (HttpServletRequest) request;

		//only open connection for special request(ex: servlet, jsp...)
		if(this.needJDBC(req)){
			System.out.println("Open Connection for: "+  req.getServletPath());

			Connection conn = null;

			try{
				//create Connection Object for connecting DB
				conn = (Connection) ConnectionUtils.getConnection();

				//control commit manually
				conn.setAutoCommit(false);

				//store Connection Object into attribute of request
				MyUtils.storeConnection(request, conn);

				//allow request to go on or to the target
				chain.doFilter(request, response);

				//finish transaction with DB
				conn.commit();
			}catch(Exception e){
				e.printStackTrace();
				ConnectionUtils.rollbackQuietly(conn);
				throw new ServletException();
			}finally{
				ConnectionUtils.closeQuietly(conn);
				System.out.println("Close Connection for: "+  req.getServletPath());
			}
		}else{ //for normal request(image, css, html,...) no need for opening connection
			//allow request to go on
			chain.doFilter(request, response);
		}


	}

}
