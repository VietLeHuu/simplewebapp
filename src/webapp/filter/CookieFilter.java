package webapp.filter;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import webapp.beans.UserAccount;
import webapp.utils.DBUtils;
import webapp.utils.MyUtils;


@WebFilter(filterName = "cookieFilter", urlPatterns = {"/*"})
public class CookieFilter implements Filter{


	public CookieFilter(){

	}

	@Override
	public void init(FilterConfig fConfig)throws ServletException{

	}

	@Override
	public void destroy(){

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException{


		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		System.out.println("Cookie Store for: "+  req.getServletPath());

		UserAccount userInSession = MyUtils.getLoggedInUser(session);

		if(userInSession !=null){
			session.setAttribute("COOKIE_CHECKED", "CHECKED");
			chain.doFilter(request, response);
			return;
		}

		Connection conn = MyUtils.getStoredConnection(request);

		String checked = (String) session.getAttribute("COOKIE_CHECKED");
		if(checked == null && conn!=null){

			System.out.println("Cookie checked? ");
			String userName = MyUtils.getUserNameFromCookie(req);
			try{
				UserAccount user = DBUtils.findUser(conn, userName);
				MyUtils.storeLoggedInUser(session, user);
			}catch(Exception e){
				e.printStackTrace();
			}

			session.setAttribute("COOKIE_CHECKED", "CHECKED");
		}

		chain.doFilter(request, response);
	}
}
