<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<div style="background: #E0E0E0; height: 55px; padding: 5px;">

	<div style="float:left;">
		<h1 class="text-primary">My Site</h1>
	</div>

	<div style="float: right; padding: 10px; text-align: right;">
		Hello<b>${loggedInUser.userName }</b>
		<br/>
		Search<input name ="search" class="form-control">
	</div>

</div>